/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_feras_1;

import java.io.IOException;
import java.util.Scanner;


/**
 *
 * @author andre
 */
public class TP_FERAS_1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        //Servico ser = new Servico();
        Locacao l;
            l = new Locacao();
            int op = 1,op1;
        
            while(op!=0){
            Scanner s = new Scanner(System.in);
                
                System.out.println("Quem eh voce?\n"
                        + "1-Cliente\n"
                        + "2-Assassino\n"
                        + "0-SAIR");
                op = s.nextInt();
                if(op == 1){
                    System.out.println("Ja possui cadastro?:\n1-Sim\n2-Nao\n0-SAIR");
                    int op2 = s.nextInt();
                    if(op2==1){
                        System.out.println("1-Alugar\n"
                            + "2-Atualizar\n"
                            + "3-Apagar cadastro\n"
                            + "0-SAIR");
                        op1 = s.nextInt();
                        switch (op1) {
                        case 1:
                            l.alugar();
                            break;
                        case 2:
                            l.atualizaCliente();
                            break;
                        case 3:
                            l.apagarCliente();
                            break;
                        default:
                            break;
                    }
                    }
                    else if(op2==2){
                        System.out.println("1-Cadastrar\n"
                            + "0-SAIR");
                        op1 = s.nextInt();
                        switch (op1) {
                        case 1:
                            l.cadastraCliente();
                            break;
                        default:
                            break;
                        }
                    }                 
                }
                if(op == 2){
                    System.out.println("1-Cadastrar\n"
                            + "2-Atualizar\n"
                            + "3-Apagar cadastro\n"
                            + "0-SAIR");
                    op1 = s.nextInt();
                    s.nextLine();
                    switch (op1) {
                        case 1:
                            l.cadastroAssassino();
                            break;
                        case 2:
                            l.atualizaAssassino();
                            break;
                        case 3:
                            l.apagaAssassino();
                            break;
                        default:
                            break;
                    }
                }
                //s.close();
            }
        
        
        l.fechar();

    }
    
}
