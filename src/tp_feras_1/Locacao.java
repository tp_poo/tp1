/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_feras_1;

import java.awt.Color;
import java.util.List;
import java.util.Scanner;
import java.util.LinkedList;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


/**
 *
 * @author andre
 */
public class Locacao {
    private List<Assassinos> catalogo;
    private int tamanho;
    private List<Assassinos> venda; 
    private int tamanhoVenda;
    private List<Cliente> clientes;
    private int numeroClientes;

    public int getTamanho() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }
    


    public Locacao() throws FileNotFoundException, IOException {
        this.catalogo = new LinkedList<>();
        this.clientes = new LinkedList<>();
        String nome , descricao, senha;
        int tipo;
        float preco;
        try (BufferedReader buffRead = new BufferedReader(new FileReader("bancodados.mb"))) {
            while (true) {
                if(buffRead.readLine()==null)break;
                 nome = buffRead.readLine();
                 preco = Float.parseFloat(buffRead.readLine());
                 tipo = Integer.parseInt(buffRead.readLine());
                 descricao = buffRead.readLine();
                 this.catalogo.add(new Assassinos(nome,preco,tipo,descricao));
                 tamanho++;
            }
            buffRead.close();
        }
        try (BufferedReader buffRead = new BufferedReader(new FileReader("bancodadosclientes.mb"))) {
            while (true) {
                if(buffRead.readLine()==null)break;
                 nome = buffRead.readLine();
                 senha = buffRead.readLine();
                 this.clientes.add(new Cliente(nome,senha));
                 numeroClientes++;
            }
            buffRead.close();
        }
        
    }
    
    
    public void cadastroAssassino(){
        Assassinos a = new Assassinos();
        a.atualiza();
        
        catalogo.add(a);
        this.tamanho++;
    }
    
    public void atualizaAssassino(){
        Scanner a = new Scanner(System.in);
        System.out.println("Nome: ");
        String nome = a.nextLine();
        for(Assassinos catalogos : catalogo){
            if(catalogos.getNome().compareTo(nome) == 0){
               catalogos.atualiza();
                break;
            }
        }
    }
    
    
    public void apagaAssassino(){
        Scanner a = new Scanner(System.in);
        while(true){
        System.out.println("Nome: ");
        String nome = a.nextLine();
        for(Assassinos catalogos : catalogo){
            if(catalogos.getNome().compareTo(nome) == 0){
               catalogo.remove(catalogos);
               tamanho--;
                System.out.println("Cadastro Removido\n");
               return ;
            }   
        }
        System.out.println("Nao existe esse nome no nosso sistema, digite seu nome novamente \n");
        }
    }
    public void fechar() throws IOException{
        try (BufferedWriter buffWrite = new BufferedWriter(new FileWriter("bancodados.mb"))) {
            for(int i = 0 ;i<tamanho;i++){
                buffWrite.append(catalogo.get(i).toString());
            }
            buffWrite.close();
        }
        try (BufferedWriter buffWrite = new BufferedWriter(new FileWriter("bancodadosclientes.mb"))) {
            for(int i = 0 ;i<numeroClientes;i++){
                buffWrite.append(clientes.get(i).toString());
            }
            buffWrite.close();
        }
    }
    public List<Assassinos> getCatalogo() {
        return catalogo;
    }
    public void cadastraCliente(){
        Cliente c = new Cliente();
        c.atualiza();
        
        clientes.add(c);
        numeroClientes++;
    }
    public void atualizaCliente(){
        Scanner a = new Scanner(System.in);
        boolean n=false;
        while(!n){
            System.out.println("Nome: \n");
            String nome = a.nextLine();
                if(nome.compareTo("-1")==0){
                break;
            }
            System.out.println("Senha: \n");
            String senha = a.nextLine();
            for(Cliente c : clientes){
                if(c.getNome().compareTo(nome) == 0&&c.getSenha().compareTo(senha)==0){
                   c.atualiza();
                    System.out.println("Cadastro atualizado\n");
                    n=true;
                   break;
                }
            }
            if(n) break;
            System.out.println("Cadastro nao encontrado digite novamente (Digite -1 para sair)\n");
            
        }
    }
    public void apagarCliente(){
        Scanner a = new Scanner(System.in);
        boolean n=false;
        while(!n){
            System.out.println("Nome: \n");
            String nome = a.nextLine();
                if(nome.compareTo("-1")==0){
                break;
            }
            System.out.println("Senha: \n");
            String senha = a.nextLine();
            for(Cliente c : clientes){
                if(c.getNome().compareTo(nome) == 0&&c.getSenha().compareTo(senha)==0){
                   clientes.remove(c);
                   System.out.println("Cadastro Removido\n");
                   numeroClientes--;
                   n=true;
                   break;
                }
            }
            if(n) break;
            System.out.println("Cadastro nao encontrado digite novamente (Digite -1 para sair)\n");
            
        }
    }
    public void alugar() throws IOException{
        this.venda = new LinkedList<>();
        String nome, nomeCliente="aaa", senha;
        int op = 1;
        Scanner s = new Scanner(System.in);
        boolean n = false;
        Cliente c = null;
        while(!n){
            System.out.println("Faca o login\nNome:");
            nomeCliente = s.nextLine();
               if(nomeCliente.compareTo("-1")==0){
               break;
               }
            System.out.println("Senha: ");
            senha = s.nextLine();
            
            for(int i = 0;i<numeroClientes;i++){
                c = clientes.get(i);
                if(c.getNome().compareTo(nomeCliente) == 0&&c.getSenha().compareTo(senha)==0){
                   n=true;
                   break;
                }
            }
            if(n)break;
            System.out.println("Cadastro nao encontrado digite novamente (Digite -1 para sair)\n");
        }
        if(nomeCliente.compareTo("-1")!=0){
            while(op == 1){

                System.out.println("Escolha o tipo: \n"
                            + "1-Assassinato simples\n" 
                            + "2-Assassinato composto\n" 
                            + "3-Sequestro relampago\n" 
                            + "4-Esquartejamento\n" 
                            + "5-Fordicidio\n"
                            + "6-Tortura\n" 
                            + "7-Outros\n");
                op = s.nextInt();
                s.nextLine();
                boolean f=false;
                for(int i = 0 ; i < tamanho ; i++){
                    if(catalogo.get(i).getTipo()==op||(catalogo.get(i).getTipo()>=7&&op>=7)){
                        System.out.println("Nome: "+catalogo.get(i).getNome()+"\nDescricao: "+catalogo.get(i).getDescricao());
                        System.out.printf("R$%.2f\n\n\n",catalogo.get(i).getPreco());
                        f = true;
                    }
                }
                if(f){
                    System.out.println("Digite o nome escolhido: \n");
                    nome = s.nextLine();
                    for(int i = 0 ; i < tamanho ; i++){
                        if(catalogo.get(i).getNome().compareTo(nome)==0){
                            this.venda.add(catalogo.get(i));
                            tamanhoVenda++;
                            break;
                        }
                    }
                }
                else System.out.println("Nao possui ninguem nessa categoria.\n");
                
                System.out.println("Deseja escolher mais alguem?\n1-Sim\n2-Finalizar escolha\n3-Cancelar compra\n");
                op = s.nextInt();
                s.nextLine();
            }
            if(op == 2){
                this.imprimeNota(c);
            }
            venda.clear();
        }
    }
    
    public void imprimeNota(Cliente c) throws IOException{
        float precoTotal = 0.f;
        try (BufferedWriter buffWrite = new BufferedWriter(new FileWriter("bancodadosvendas.mb",true))) {
            
        
        System.out.println("-------------------------------"
                        + "\nNome Cliente: "+c.getNome()+"\n\n");
        buffWrite.append("-------------------------------"
                        + "\nNome Cliente: "+c.getNome()+"\n\n");
        for(int i = 0 ; i< tamanhoVenda;i++){
            System.out.println((i+1)+" - Nome: "+venda.get(i).getNome()+"\nTipo: "+venda.get(i).getTipoNome());
            buffWrite.append((i+1)+" - Nome: "+venda.get(i).getNome()+"\nTipo: "+venda.get(i).getTipoNome()+"\n"+venda.get(i).getPreco()+"\n");
            System.out.printf("R$%.2f\n\n",venda.get(i).getPreco());
            precoTotal+=venda.get(i).getPreco();
        }
        System.out.printf("\nTotal: R$%.2f\n",precoTotal);
        System.out.println("-------------------------------");
        buffWrite.append("-------------------------------\n");
            buffWrite.close();
        }
    }
    
    
}
