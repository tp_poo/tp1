/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_feras_1;

import java.util.Scanner;

/**
 *
 * @author andre
 */
public class Cliente {
    private String nome;
    private String senha;

    Cliente() {
        
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    @Override
    public String toString(){
        return "\n" + this.nome + "\n" + this.senha + "\n";
    }
    
    public void atualiza(){
        System.out.println("Digite seu novo nome:\n");
        Scanner s = new Scanner(System.in);
        nome = s.nextLine();
        System.out.println("Digite sua nova senha: \n");
        senha = s.nextLine();
    }

    public Cliente(String nome, String senha) {
        this.nome = nome;
        this.senha = senha;
    }
    
}
